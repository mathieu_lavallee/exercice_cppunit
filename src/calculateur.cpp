#include "calculateur.h"

int Calculateur::calculer(int v1, int v2) {
	int resultat = 0;
	if (v1<0) v1 = -v1;
	for(int i=0 ; i<v1 ; i++) {
		if (v2>0) resultat += v2;
		else resultat -= v2;
	}	
	return resultat;
}

