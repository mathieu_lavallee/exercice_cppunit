#include <iostream>

#include "calculateur.h"

int main(int argc, char** argv) {

	if (argc<3) {
		std::cout << "Il faut fournir deux paramètres entiers";
	       	std::cout << std::endl;
		return -1;
	}

	int valeur1 = atoi(argv[1]);
	int valeur2 = atoi(argv[2]);

	Calculateur* calculatrice = new Calculateur();
	int resultat = calculatrice->calculer(valeur1, valeur2);

	std::cout << valeur1 << " et " << valeur2;
	std::cout << " donne un résultat de " << resultat << std::endl;

	delete calculatrice;

	return 0;
}


