# Exécutables
EXEC=exercice
EXECTEST=exercice_test
# Répertoires
SOURCE=src
BINAIRE=bin
TESTS=test

all:launch

# =======================
# Compilation du logiciel
# =======================

$(BINAIRE)/$(EXEC): $(BINAIRE)/main.o $(BINAIRE)/calculateur.o
	g++ -o $@ $^

$(BINAIRE)/main.o: $(SOURCE)/main.cpp $(SOURCE)/calculateur.h
	mkdir -p $(BINAIRE)
	g++ -o $@ -c $<

$(BINAIRE)/calculateur.o: $(SOURCE)/calculateur.cpp $(SOURCE)/calculateur.h
	g++ -o $@ -c $<

launch: $(BINAIRE)/$(EXEC)
	./$(BINAIRE)/$(EXEC) 161 13

# ===========
# Utilitaires
# ===========

clean:
	rm -rf $(BINAIRE)/*.o
	rm -rf $(BINAIRE)/$(EXEC)
	rm -rf $(TESTS)/$(BINAIRE)/*.o
	rm -rf $(TESTS)/$(BINAIRE)/$(EXECTEST)

# =================
# Tests du logiciel
# =================

test: $(TESTS)/$(BINAIRE)/$(EXECTEST)
	./$(TESTS)/$(BINAIRE)/$(EXECTEST)

$(TESTS)/$(BINAIRE)/$(EXECTEST): $(TESTS)/$(BINAIRE)/main.o $(TESTS)/$(BINAIRE)/calculateur_test.o $(TESTS)/$(BINAIRE)/calculateur.o
	g++ -o $@ $^ -lcppunit

$(TESTS)/$(BINAIRE)/main.o: $(TESTS)/$(SOURCE)/main.cpp $(TESTS)/$(SOURCE)/calculateur_test.h
	mkdir -p $(TESTS)/$(BINAIRE)
	g++ -o $@ -c $<

$(TESTS)/$(BINAIRE)/calculateur_test.o: $(TESTS)/$(SOURCE)/calculateur_test.cpp $(TESTS)/$(SOURCE)/calculateur_test.h $(SOURCE)/calculateur.h
	g++ -o $@ -c $<

$(TESTS)/$(BINAIRE)/calculateur.o: $(SOURCE)/calculateur.cpp $(SOURCE)/calculateur.h
	g++ -o $@ -c $<

