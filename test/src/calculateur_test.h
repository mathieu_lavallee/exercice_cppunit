// Classe qui teste la classe Diviseur
// Avec le framework CppUnit

// Librairies CppUnit nécessaires.
#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

// Le fichier à tester, qui se trouve dans un répertoire différent.
#include "../../src/calculateur.h"

class CalculateurTest : public CppUnit::TestFixture
{
	CPPUNIT_TEST_SUITE(CalculateurTest);
    	CPPUNIT_TEST(test_propre);
    	CPPUNIT_TEST_SUITE_END();
    
private:
	Calculateur* objet_a_tester;
    
public:
	// Fonctions d'échafaudage
    	void setUp();
	void tearDown();
    
    	// Fonctions de tests
    void test_propre();
};




